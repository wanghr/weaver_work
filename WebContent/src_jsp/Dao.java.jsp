<%@page language="java" import="java.util.*" pageEncoding="GBK"%>
<%@page import="database.area.*"%>
<%@include file="/params/student.jsp"%>
<%
    List<Column> cols = AppArea.table.cols;
    String id = AppArea.table.primaryKey;
    String t_tablename = AppArea.table.name;
    String tablename = Util.getNoprefixAndLower(t_tablename, "t_");
    String Tablename = Util.getFirstUpper(tablename);
%>
<!-- -----------------------headsplit---------------------- -->
package dao.<%=tablename%>;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import Test.StringUtil;
import Test.TestDB;

public class Dao extends TestDB{
//  ssss
    public HashMap Search<%=Tablename%>s(String key, int index, int size, String sortField, String sortOrder) throws Exception
    {
        //System.Threading.Thread.Sleep(300);
    if(key == null) key = "";
    String sql =
    "select a.*\n"
    <%-- "select a.*, b.name dept_name, c.name position_name, d.name educational_name \n"--%>
           +"from <%=t_tablename%> a \n"
           <%--
           +"left join t_department b \n"
           +"on a.dept_<%=id%> = b.<%=id%> \n"
           +"left join t_position c \n"
           +"on a.position = c.<%=id%> \n"
           +"left join t_educational d \n"
           +"on a.educational = d.<%=id%> \n"--%>
           +"where a.name like '%" + key + "%' \n";
        if (StringUtil.isNullOrEmpty(sortField) == false)
        {
            if ("desc".equals(sortOrder) == false) sortOrder = "asc";
            sql += " order by " + sortField + " " + sortOrder;
        }
        else
        {
            sql += " order by createtime desc";
        }
        ArrayList dataAll = DBSelect(sql);
        ArrayList data = new ArrayList();
        int start = index * size, end = start + size;
        for (int i = 0, l = dataAll.size(); i < l; i++)
        {
            HashMap record = (HashMap)dataAll.get(i);
            if (record == null) continue;
            if (start <= i && i < end)
            {
                data.add(record);
            }
            record.put("createtime", new Timestamp(100,10,10,1,1,1,1));
        }
        HashMap result = new HashMap();
        result.put("data", data);
        result.put("total", dataAll.size());
        //minAge, maxAge, avgAge
        ArrayList ages = DBSelect("select min(age) as minAge, max(age) as maxAge, avg(age) as avgAge from <%=t_tablename%>");
        HashMap ageInfo = (HashMap)ages.get(0);
        result.put("minAge", ageInfo.get("minAge"));
        result.put("maxAge", ageInfo.get("maxAge"));
        result.put("avgAge", ageInfo.get("avgAge"));
        return result;
    }

    public HashMap Get<%=Tablename%>(String <%=id%>) throws Exception
    {
    	String sql = "select * from t_<%=tablename%> where <%=id%> = '"+<%=id%>+"'";
        ArrayList data = DBSelect(sql);
        return data.size() > 0 ? (HashMap)data.get(0) : null;
    }

    public String Insert<%=Tablename%>(HashMap user) throws Exception
    {
    	String <%=id%> = (user.get("<%=id%>") == null || user.get("<%=id%>").toString().equals(""))? UUID.randomUUID().toString() : user.get("<%=id%>").toString();
        user.put("<%=id%>", <%=id%>);
        if (user.get("name") == null) user.put("name", "");
        if (StringUtil.isNullOrEmpty(user.get("gender"))) user.put("gender", 0);
    	Connection conn = TestDB.getConn();
    	String sql = "insert into t_<%=tablename%> (<%String s1 = ""; for(int i=0;i<cols.size();i++){s1+=i!=0?","+cols.get(i).Code:cols.get(i).Code;}%><%=s1%>)"
       	 + " values(<%
           String s = "";
           for(int i=0;i<cols.size();i++){
              s += i!=0?",?" : "?";
           }%><%=s%>)";
       PreparedStatement stmt = conn.prepareStatement(sql);
       <%for(int i=0;i<cols.size();i++){
              Column col = cols.get(i);
              if(Util.likein(col.DataType.toLowerCase(),new String[]{"varchar","char"})){%>
                  stmt.setString(<%=i+1%>,ToString(user.get("<%=col.Code%>")));
              <%}
              if(Util.likein(col.DataType.toLowerCase(),new String[]{"int","long"})){%>
                  stmt.setInt(<%=i+1%>,ToInt(user.get("<%=col.Code%>")));
              <%}
              if(Util.likein(col.DataType.toLowerCase(),new String[]{"datetime"})){%>
                  stmt.setDate(<%=i+1%>,ToDate(user.get("<%=col.Code%>")));
              <%}
           }
       %>
       stmt.executeUpdate();
        stmt.close();
       conn.close();
        return <%=id%>;
    }

    public void Delete<%=Tablename%>(String userId) throws Exception
    {
       Connection conn = getConn();
       Statement stmt = conn.createStatement();
        String sql = "delete from t_<%=tablename%> where <%=id%> = \""+userId+"\"";
        stmt.executeUpdate(sql);
       stmt.close();
       conn.close();
    }

    public void Update<%=Tablename%>(HashMap user) throws Exception
    {
        HashMap db_user = Get<%=Tablename%>(user.get("<%=id%>").toString());
        Iterator iter = user.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            Object key = entry.getKey();
            Object val = entry.getValue();
            db_user.put(key, val);
        }
        Delete<%=Tablename%>(user.get("<%=id%>").toString());
        Insert<%=Tablename%>(db_user);
    }


}

 
