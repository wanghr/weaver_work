<%@ page language="java" import="test.*" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body><%
	String work = request.getParameter("work");
	String project_path = request.getParameter("project_path");
	
	if("show".equals(work)){
		out.print(session.getAttribute("info"));
		session.setAttribute("info","");
	}else if("toJsp".equals(work)){
		try{
			out.print("...");
			ToolMain.convertJsp(project_path);
			session.setAttribute("info","转换成功");
		}catch(Exception ex){
			ex.printStackTrace();
			session.setAttribute("info","转换出错");
		}
		response.sendRedirect("convert.jsp?work=show");
	}else if("toSrc".equals(work)){
		session.setAttribute("info","转换成功");
		try{
			ToolMain.getNewSource(project_path);
			session.setAttribute("info","转换成功");
		}catch(Exception ex){		
			session.setAttribute("info","转换出错");
		}
		response.sendRedirect("convert.jsp?work=show");
	}
%></body>
</html>