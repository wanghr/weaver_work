package test;

import java.io.File;

import cc.ifelsefor.twoj.core.JspConverterTool;
import cc.ifelsefor.twoj.core.URLConnectGetTool;

public class ToolMain {
	
	/**
	 * Source(*.*) ===> Source_jsp(*.*.jsp)
	 *  将指定类型的源代码文件初步转换成JSP模板文本,文件名在源文件名后追加.jsp例如:Dao.java->Dao.java.jsp,
	 *  将源文本中包含有本身和JSP动态标记冲突的字符按照reps替换,例如源代码中包含"<%"这和JSP动态标记"<%"冲突,所以
	 *  规定将<%转换成<`%;
	 * @throws Exception
	 */
	public static void convertJsp(String basePath) throws Exception{
		/* 将Source目录下的文件类型为html,java,jsp的文件转换成jsp,保存在Source_jsp目录中 */
		// 源代码文件夹
		File srcmod = new File(basePath+"\\WebContent\\src_old");
		// 模板保存文件夹
		File jspmod = new File(basePath+"\\WebContent\\src_jsp");
		//您也可以使用绝对路径,像下面这样
		//File srcmod = new File("C:\\Users\\Administrator\\workspace\\weaver_work\\WebContent\\src");
		//File jspmod = new File("C:\\Users\\Administrator\\workspace\\weaver_work\\WebContent\\src_jsp");
		
		// 源代码哪些文件要被转换成jsp 
		String[] fileFilter = {"txt","html","java","jsp"};
		// 声明在源文件转换成JSP文件过程中为了避免源文件中包含的字符与JSP动态标记冲突而定义的替换规则 
		String[][] reps = new String[][]{{"${","$`{"},{"#{","#`{"},{"<%","<`%"},{"<jsp:","<`jsp:"},{"</jsp:","</`jsp:"}};
		
		// 创建转换器并执行转换
		JspConverterTool jcv = new JspConverterTool(srcmod, jspmod, fileFilter, reps);
		jcv.convertSrcToJsp();
	}
	
	// 请求服务器源代码模板,获得新的源代码
	public static void getNewSource(String basePath) throws Exception{
		/* 将Source目录下的文件类型为html,java,jsp的文件转换成jsp,保存在Source_jsp目录中 */
		// 模板代码文件夹
		File jspmod = new File(basePath+"\\WebContent\\src_jsp");
		// 新源代码保存文件夹
		File newsrcMod = new File(basePath+"\\WebContent\\src_new");
		//您也可以使用绝对路径,像下面这样
		//File jspmod = new File("C:\\Users\\Administrator\\workspace\\weaver_work\\WebContent\\src_jsp");
		//File newsrcMod = new File("C:\\Users\\Administrator\\workspace\\weaver_work\\WebContent\\src_new");
		
		// 根据替换规则将发生冲突转换后的字符串反转换还原
		String[][] reps = new String[][]{{"${","$`{"},{"#{","#`{"},{"<%","<`%"},{"<jsp:","<`jsp:"},{"</jsp:","</`jsp:"}};
		// JSP服务器工程http://localhost:8080/weaver_work/
		URLConnectGetTool msg = new URLConnectGetTool("http://localhost:8080/weaver_work/", reps, newsrcMod, jspmod);
		
		//通过请求服务器工程运行模板获取源代码
		msg.getSourceFromJsp();
		
	}
	
	// 程序运行操作
	public static void main(String[] args) throws Exception {
		// 你也可以直接运行Jsp
		
		//(1)将旧源代码转换成JSP
		//convertJsp("");
		//(2)从服务器引擎运行JSP,得到新源代码
		getNewSource("");
		
	}
	
	
}
